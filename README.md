# Sysprogazers

Listen to my song

## App Description
Webapp that controls network card. 

## How to Build
- Make sure your system run on linux
- Install ethtool, python3, python3-pip, and python3-venv first.
  If you use ubuntu/debian-based linux distro, you can install those dependencies by using 
  ```
  sudo apt update
  sudo apt install python3 python3-pip python3-venv ethtool
  ```
- Once those dependencies are installed,  go to drivers folders then run 
  ```
  make
  sudo insmod sysprogazers_drv.ko
  ```
- After that change /home/eagle/sysprogazers in the Makefile (on the project root one) and the runserserver.service to absolute path where you store the sysprogazers folder.
- After that change the password input in config.sh and kill.sh from  ```sysprog2019``` to your own password.
- Copy the bootscript (runserver.service) to /etc/systemd/system by using 
  ```
  sudo cp service/runserver.service /etc/systemd/system
  ```
## How to Run
- Run this command to start the application
  ```
  sudo systemctl start runserver.service
  ```
- To make the app run automatically on startup, run this command
  ```
  sudo systemctl enable runserver.service
  ```
- You can the webapp on your browser by visiting localhost:8000

## Contributors
Sysprogazers group
1. Nadhif Adyatma Prayoga (1806205501) @EaglesCommander
2. Rocky Arkan Adnan Ahmad (1806186566) @racendol
3. Mahardika Krisna Ihsani  (1806141284) @codefire53
