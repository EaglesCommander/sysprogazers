from django import forms
from django.contrib.auth.forms import AuthenticationForm

class LoginForm(forms.Form):
    attrs = {
        'class':'form-control w-75'
    }

    username = forms.CharField(widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=attrs))