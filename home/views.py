from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
import subprocess
import time

# Create your views here.

DEFAULT = "Choose..."
sudo_password = "sysprog2019"

def index(request):
    return render(request, 'home.html')

def change_config(request):
    autoneg = request.POST.get("autonegInput")
    if autoneg == DEFAULT:
        speed = request.POST.get("speedInput")
        duplex = request.POST.get("duplexInput")

        if speed == DEFAULT and duplex == DEFAULT:
            return redirect("/")

        elif speed == DEFAULT:
            subprocess.run(["./scripts/config.sh", f'duplex {duplex} autoneg off'])

        elif duplex == DEFAULT:
            subprocess.run(["./scripts/config.sh", f'speed {speed} autoneg off'])

        else:
            subprocess.run(["./scripts/config.sh", f'speed {speed} duplex {duplex} autoneg off'])

    else:
        subprocess.run(["./scripts/config.sh", f'autoneg {autoneg}'])
 
    return redirect("/")

def kill_dash_nine(request):
    subprocess.run(["nohup", "./scripts/kill.sh"])

    return redirect("/")
    
def ping(request):
    output = subprocess.run(["./scripts/ping.sh"])
    if (output.returncode == 0):
    	return render(request, 'success.html')
    else:
    	return render(request, 'failure.html')
    	
