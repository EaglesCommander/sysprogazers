from django.urls import path
from .views import index, change_config, kill_dash_nine, ping

urlpatterns = [
    path('', index, name = 'home'),
    path('submit', change_config, name = 'submit'),
    path('kill', kill_dash_nine, name = 'kill'),
    path('ping', ping, name = 'ping'),
]
